EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:wemos_mini
LIBS:wemos-breakout-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L WeMos_mini U1
U 1 1 5B2D0221
P 4150 3250
F 0 "U1" H 4150 3750 60  0000 C CNN
F 1 "WeMos_mini" H 4150 2750 60  0000 C CNN
F 2 "wemos_d1_mini:D1_mini_board" H 4700 2550 60  0001 C CNN
F 3 "" H 4700 2550 60  0000 C CNN
	1    4150 3250
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG01
U 1 1 5B2D02B9
P 700 850
F 0 "#FLG01" H 700 925 50  0001 C CNN
F 1 "PWR_FLAG" H 700 1000 50  0000 C CNN
F 2 "" H 700 850 50  0001 C CNN
F 3 "" H 700 850 50  0001 C CNN
	1    700  850 
	-1   0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG02
U 1 1 5B2D02CF
P 1200 750
F 0 "#FLG02" H 1200 825 50  0001 C CNN
F 1 "PWR_FLAG" H 1200 900 50  0000 C CNN
F 2 "" H 1200 750 50  0001 C CNN
F 3 "" H 1200 750 50  0001 C CNN
	1    1200 750 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5B2D02E5
P 1200 850
F 0 "#PWR03" H 1200 600 50  0001 C CNN
F 1 "GND" H 1200 700 50  0000 C CNN
F 2 "" H 1200 850 50  0001 C CNN
F 3 "" H 1200 850 50  0001 C CNN
	1    1200 850 
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR04
U 1 1 5B2D02FB
P 700 750
F 0 "#PWR04" H 700 600 50  0001 C CNN
F 1 "VCC" H 700 900 50  0000 C CNN
F 2 "" H 700 750 50  0001 C CNN
F 3 "" H 700 750 50  0001 C CNN
	1    700  750 
	1    0    0    -1  
$EndComp
Wire Wire Line
	700  750  700  850 
Wire Wire Line
	1200 750  1200 850 
$Comp
L Conn_01x07 J7
U 1 1 5B2D0F62
P 5350 3300
F 0 "J7" H 5350 3700 50  0000 C CNN
F 1 "Right_Pins" H 5350 2900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x07_Pitch2.54mm" H 5350 3300 50  0001 C CNN
F 3 "" H 5350 3300 50  0001 C CNN
	1    5350 3300
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x06 J3
U 1 1 5B2D103F
P 3000 3400
F 0 "J3" H 3000 3700 50  0000 C CNN
F 1 "Left_Pins" H 3000 3000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 3000 3400 50  0001 C CNN
F 3 "" H 3000 3400 50  0001 C CNN
	1    3000 3400
	-1   0    0    1   
$EndComp
$Comp
L Conn_01x07 J4
U 1 1 5B2D1106
P 3150 2100
F 0 "J4" H 3150 2550 50  0000 C CNN
F 1 "5V" H 3150 1700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x07_Pitch2.54mm" H 3150 2100 50  0001 C CNN
F 3 "" H 3150 2100 50  0001 C CNN
	1    3150 2100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3450 2300 3450 2450
Wire Wire Line
	2850 2300 3450 2300
Connection ~ 3050 2300
Connection ~ 3150 2300
Connection ~ 3250 2300
Connection ~ 3350 2300
$Comp
L Conn_01x07 J2
U 1 1 5B2D1222
P 2300 2900
F 0 "J2" H 2300 3200 50  0000 C CNN
F 1 "GND2" H 2300 2500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x07_Pitch2.54mm" H 2300 2900 50  0001 C CNN
F 3 "" H 2300 2900 50  0001 C CNN
	1    2300 2900
	-1   0    0    1   
$EndComp
Wire Wire Line
	3400 3000 3650 3000
Wire Wire Line
	3400 2600 3400 3000
Wire Wire Line
	2500 2600 3400 2600
Wire Wire Line
	2500 2600 2500 4050
Connection ~ 2500 3000
Connection ~ 2500 2900
Connection ~ 2500 2800
Connection ~ 2500 2700
Wire Wire Line
	3650 3100 3200 3100
Wire Wire Line
	3200 3200 3650 3200
Wire Wire Line
	3650 3300 3200 3300
Wire Wire Line
	3200 3400 3650 3400
Wire Wire Line
	3650 3500 3200 3500
Wire Wire Line
	3200 3600 3650 3600
$Comp
L Conn_01x02 J5
U 1 1 5B2D1473
P 4150 2200
F 0 "J5" H 4150 2300 50  0000 C CNN
F 1 "PWR_IN" H 4150 2000 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Angled_1x02_Pitch2.54mm" H 4150 2200 50  0001 C CNN
F 3 "" H 4150 2200 50  0001 C CNN
	1    4150 2200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3550 3000 3550 2500
Wire Wire Line
	3550 2500 4150 2500
Wire Wire Line
	4150 2500 4150 2400
Connection ~ 3550 3000
Wire Wire Line
	4250 2450 4250 2400
Wire Wire Line
	4650 3000 5150 3000
Wire Wire Line
	5150 3100 4650 3100
Wire Wire Line
	4650 3200 5150 3200
Wire Wire Line
	5150 3300 4650 3300
Wire Wire Line
	4650 3400 5150 3400
Wire Wire Line
	5150 3500 4650 3500
$Comp
L Conn_01x07 J1
U 1 1 5B2D16B8
P 2300 3700
F 0 "J1" H 2300 4000 50  0000 C CNN
F 1 "GND1" H 2300 3300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x07_Pitch2.54mm" H 2300 3700 50  0001 C CNN
F 3 "" H 2300 3700 50  0001 C CNN
	1    2300 3700
	-1   0    0    1   
$EndComp
Connection ~ 2500 3100
Connection ~ 2500 3800
Connection ~ 2500 3700
Connection ~ 2500 3600
Connection ~ 2500 3500
Connection ~ 2500 3400
$Comp
L Conn_01x07 J6
U 1 1 5B2D175A
P 5100 2250
F 0 "J6" H 5100 2700 50  0000 C CNN
F 1 "3.3V" H 5100 1850 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x07_Pitch2.54mm" H 5100 2250 50  0001 C CNN
F 3 "" H 5100 2250 50  0001 C CNN
	1    5100 2250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4650 2900 4800 2900
Wire Wire Line
	4800 2900 4800 2450
Wire Wire Line
	4800 2450 5400 2450
Connection ~ 4900 2450
Connection ~ 5000 2450
Connection ~ 5100 2450
Connection ~ 5200 2450
$Comp
L GND #PWR05
U 1 1 5B31A92B
P 2500 4050
F 0 "#PWR05" H 2500 3800 50  0001 C CNN
F 1 "GND" H 2500 3900 50  0000 C CNN
F 2 "" H 2500 4050 50  0001 C CNN
F 3 "" H 2500 4050 50  0001 C CNN
	1    2500 4050
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR06
U 1 1 5B31A99E
P 3300 2450
F 0 "#PWR06" H 3300 2300 50  0001 C CNN
F 1 "VCC" H 3300 2600 50  0000 C CNN
F 2 "" H 3300 2450 50  0001 C CNN
F 3 "" H 3300 2450 50  0001 C CNN
	1    3300 2450
	0    -1   -1   0   
$EndComp
$Comp
L R R1
U 1 1 5B4BA47F
P 3650 2700
F 0 "R1" V 3730 2700 50  0000 C CNN
F 1 "0 Ω" V 3650 2700 50  0000 C CNN
F 2 "Connect:GS2" V 3580 2700 50  0001 C CNN
F 3 "" H 3650 2700 50  0001 C CNN
	1    3650 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 2450 4250 2450
Wire Wire Line
	3650 2450 3650 2550
Connection ~ 3450 2450
Connection ~ 3650 2450
Wire Wire Line
	3650 2850 3650 2900
Connection ~ 5300 2450
Connection ~ 2950 2300
Connection ~ 2500 3900
Connection ~ 2500 4000
Connection ~ 2500 3200
Wire Wire Line
	4650 3600 5150 3600
$EndSCHEMATC
