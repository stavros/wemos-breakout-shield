WeMos Breakout Shield
=====================

This is a small shield that provides breakouts for all the GPIOs of the WeMos D1 mini, as well as rails for 5V/3.3V/GND.
I made it because I didn't want to have to use a breadboard just to get access to more power pins to connect some
sensors.


Notes
-----

The idea is that you'll solder some headers on the shield, put the WeMos in the middle and have something much more
convenient for plugging stuff in. Suggested header placement is:

* 7 straight male headers on one of the GNDs. Which one is up to you, and whether you want to use servos. If you do,
  you'll want to pick the rail next to the Vcc.
* 7 straight female headers on the other GND. I swear I have nothing against homosexuals.
* 2 angled male or female headers for the power input.
* 4 straight male and 3 straight female headers on the 3.3V, for maximum convenience. Vary the actual numbers depending
  on your needs.
* Same as above on the 5V.

There is a small 0805 pad on the right side. Shorting this pad with some solder connects the Vcc rail/power to the USB
of the WeMos. This is handy if you need to use a larger voltage on the Vcc and want to power the WeMos through USB, so
you want the Vcc and the WeMos USB voltage to remain separate, but you still have the option of connecting them up by
shorting that pad. Doing this will allow you to power the WeMos from the "power" connector.

Images
------

Well, here:

![](images/board.png)

![](images/front.png)

![](images/back.png)
